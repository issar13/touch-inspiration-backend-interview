import { Test, TestingModule } from '@nestjs/testing';
import { TransactionService } from './transactions.service';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Wallet } from '../../db/entities/wallet.entity';
import { Transaction } from '../../db/entities/transaction.entity';

describe('TransactionService', () => {
  let transactionService: TransactionService;
  let transactionRepository: Repository<Transaction>;
  let walletRepository: Repository<Wallet>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TransactionService,
        {
          provide: getRepositoryToken(Transaction),
          useValue: {
            save: jest.fn().mockResolvedValue({}),
          },
        },
        {
          provide: getRepositoryToken(Wallet),
          useValue: {
            findOneBy: jest.fn().mockResolvedValue({ id: 1, balance: 100 }),
            save: jest.fn().mockResolvedValue({}),
          },
        },
      ],
    }).compile();

    transactionService = module.get<TransactionService>(TransactionService);
    transactionRepository = module.get<Repository<Transaction>>(getRepositoryToken(Transaction));
    walletRepository = module.get<Repository<Wallet>>(getRepositoryToken(Wallet));
  });

  describe('addTransaction', () => {
    it('should add a transaction and update wallet balance', async () => {
      const data = { type: 'expense', amount: 50, description: 'shopping' };
      const result = await transactionService.addTransaction(1, data);
      expect(result).toEqual({data:{transactionCompleted:{},walletUpdated:{}}, message: 'transaction recorded successfully'});
      expect(transactionRepository.save).toHaveBeenCalled();
      expect(walletRepository.findOneBy).toHaveBeenCalledWith({ id: 1 });
      expect(walletRepository.save).toHaveBeenCalled();
    });
  });

  describe('addTransaction', () => {
    it('should throw error if wallet does not exist', async () => {
      walletRepository.findOneBy = jest.fn().mockResolvedValue(
        null);
      try {
        const data = { type: 'expense', amount: 50, description: 'shopping' };
        await transactionService.addTransaction(1, data);
      } catch (error) {
        expect(error.message).toEqual('Wallet does not exit');
        expect(error.statusCode).toEqual(409);
      }
    });
  });
});

