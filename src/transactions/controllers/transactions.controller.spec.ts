import { Test, TestingModule } from '@nestjs/testing';
import { TransactionController } from './transactions.controller';
import { TransactionService } from '../services/transactions.service';

describe('TransactionController', () => {
  let transactionController: TransactionController;
  let transactionService: TransactionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TransactionController],
      providers: [
        TransactionService,
        {
          provide: TransactionService,
          useValue: {
            addTransaction: jest.fn().mockResolvedValue({ message: 'Transaction added successfully' }),
          },
        },
      ],
    }).compile();

    transactionService = module.get<TransactionService>(TransactionService);
    transactionController = module.get<TransactionController>(TransactionController);
  });

  describe('addTransaction', () => {
    it('should add a new transaction', async () => {
      const data = { amount: 100, type: 'income', description: 'fees' };
      const result = await transactionController.addTransaction(1, data);
      expect(result).toEqual({ message: 'Transaction added successfully' });
      expect(transactionService.addTransaction).toHaveBeenCalledWith(1, data);
    });
  });
});
