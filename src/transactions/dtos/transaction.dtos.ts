import { IsNumber, IsString, IsEnum } from 'class-validator';

export class AddTransactionDto {
  @IsEnum(['income', 'expense'])
    type: string;

  @IsNumber()
  amount: number;

  @IsString()
  description: string;
}
