import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from '../services/users.service';

describe('UsersController', () => {
  let usersController: UsersController;
  let usersService: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        {
          provide: UsersService,
          useValue: {
            findUserById: jest.fn().mockResolvedValue({ id: 1, name: 'John Doe' }),
            register: jest.fn().mockResolvedValue({ message: 'User registered successfully' }),
          },
        },
      ],
    }).compile();

    usersService = module.get<UsersService>(UsersService);
    usersController = module.get<UsersController>(UsersController);
  });

  describe('findUserById', () => {
    it('should return a user by id', async () => {
      const result = await usersController.findUserById(1);
      expect(result).toEqual({ id: 1, name: 'John Doe' });
      expect(usersService.findUserById).toHaveBeenCalledWith(1);
    });
  });

  describe('register', () => {
    it('should register a user', async () => {
      const body = { username: 'John Doe', email: 'johndoe@example.com', firstName: 'password', lastName: "ali", balance: 100 };
      const result = await usersController.register(body);
      expect(result).toEqual({ message: 'User registered successfully' });
      expect(usersService.register).toHaveBeenCalledWith(body);
    });
  });
});
