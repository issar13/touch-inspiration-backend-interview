import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../../db/entities/user.entity';

describe('UsersService', () => {
  let usersService: UsersService;
  let userRepository: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            findOne: jest.fn().mockResolvedValue(null),
            findOneBy: jest.fn().mockResolvedValue({ id: 1, firstName: 'John', lastName: 'Doe', email: 'johndoe@example.com', balance: 100 }),
            save: jest.fn().mockResolvedValue({ id: 1, firstName: 'John', lastName: 'Doe', email: 'johndoe@example.com', balance: 100 }),
          },
        },
      ],
    }).compile();

    usersService = module.get<UsersService>(UsersService);
    userRepository = module.get<Repository<User>>(getRepositoryToken(User));
  });

  describe('register', () => {
    it('should register a user', async () => {
      const body = { username: 'John Doe', email: 'johndoe@example.com', firstName: 'password', lastName: "ali", balance: 100 };
      const result = await usersService.register(body);
      expect(result).toEqual({data:{createdUser:{ id: 1, firstName: 'John', lastName: 'Doe', email: 'johndoe@example.com', balance: 100 }},message: 'user created successfully' });
      expect(userRepository.findOne).toHaveBeenCalledWith({ where: { email: 'johndoe@example.com' } });
      expect(userRepository.save).toHaveBeenCalled();
    });
    it('should throw error if user already exists', async () => {
      userRepository.findOne = jest.fn().mockResolvedValue({});
      try {
      const body = { username: 'John Doe', email: 'johndoe@example.com', firstName: 'password', lastName: "ali", balance: 100 };
        await usersService.register(body);
      } catch (error) {
        expect(error.message).toEqual('User already exists!');
        expect(error.statusCode).toEqual(409);
      }
    });
  });

  describe('findUserById', () => {
    it('should return a single user by id', async () => {
      const result = await usersService.findUserById(1);
      expect(result).toEqual({data:{singleUser:{ id: 1, firstName: 'John', lastName: 'Doe', email: 'johndoe@example.com', balance: 100 }}, message:"single user fetched"});
      expect(userRepository.findOneBy).toHaveBeenCalledWith({id: 1});
    });
    it('should return a message if user does not exist', async () => {
      userRepository.findOneBy = jest.fn().mockResolvedValue(null);
      const result = await usersService.findUserById(1);
      expect(result).toEqual({message: "There is no user"});
      expect(userRepository.findOneBy).toHaveBeenCalledWith({id: 1});
    });
  });
});
