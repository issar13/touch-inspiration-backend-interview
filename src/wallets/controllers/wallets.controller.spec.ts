import { Test, TestingModule } from '@nestjs/testing';
import { WalletsController } from './wallets.controller';
import { WalletsService } from '../services/wallets.service';

describe('WalletsController', () => {
  let walletsController: WalletsController;
  let walletsService: WalletsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WalletsController],
      providers: [
        WalletsService,
        {
          provide: WalletsService,
          useValue: {
            getWallet: jest.fn().mockResolvedValue([{ name: 'wallet1' }]),
            createWallet: jest.fn().mockResolvedValue({ message: 'Wallet created successfully' }),
            getWalletById: jest.fn().mockResolvedValue({ name: 'wallet2', balance: 50 }),
          },
        },
      ],
    }).compile();

    walletsService = module.get<WalletsService>(WalletsService);
    walletsController = module.get<WalletsController>(WalletsController);
  });

  describe('getWallets', () => {
    it('should return all wallets related to a user', async () => {
      const result = await walletsController.getWallets(1);
      expect(result).toEqual([{ name: 'wallet1' }]);
      expect(walletsService.getWallet).toHaveBeenCalledWith(1);
    });
  });

  describe('createWallet', () => {
    it('should create a wallet', async () => {
      const dto = { walletName: 'new wallet', balance: 100, userId: 5 };
      const result = await walletsController.createWallet(1, dto);
      expect(result).toEqual({ message: 'Wallet created successfully' });
      expect(walletsService.createWallet).toHaveBeenCalledWith(1, dto);
    });
  });

  describe('getWalletById', () => {
    it('should return a single wallet', async () => {
      const result = await walletsController.getWalletById(1, 2);
      expect(result).toEqual({ name: 'wallet2', balance: 50 });
      expect(walletsService.getWalletById).toHaveBeenCalledWith(1, 2);
    });
  });
});
